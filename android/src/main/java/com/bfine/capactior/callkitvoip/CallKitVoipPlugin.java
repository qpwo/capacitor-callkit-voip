package com.bfine.capactior.callkitvoip;

import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;

@CapacitorPlugin(name = "CallKitVoip")
public class CallKitVoipPlugin extends Plugin {

    private CallKitVoip implementation = new CallKitVoip();
    
    @PluginMethod
    public void register(PluginCall call) {
        System.out.println("Test statement from java euwifbn");
        call.unimplemented("Not implemented on Android.");
    }

    @PluginMethod
    public void echo(PluginCall call) {
        String value = call.getString("value");

        JSObject ret = new JSObject();
        ret.put("value", implementation.echo(value));
        call.resolve(ret);
    }
}
